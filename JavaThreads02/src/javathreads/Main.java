/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javathreads;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author human
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        MyRun myRun = new MyRun();

        Thread thread = new Thread(myRun, "Second-Thread");
        thread.start();
        
        Thread currentThread = Thread.currentThread();

        for (int i = 0; i < 100; i++) {
            System.out.println(currentThread.getName() + ": " + i);
            
            try {
                Thread.sleep(1);
            } catch (InterruptedException ex) {
                Logger.getLogger(MyRun.class.getName()).log(Level.SEVERE, null, ex);
                return;
            }
        }
    }

}
