/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package urlalla;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author All
 */
public class NewThreads implements Runnable {

    private String address;
    private File downloads;

    public NewThreads(String address, File downloads) {
        this.address = address;
        this.downloads = downloads;
    }

    @Override
    public void run() {
        try {
            URLConnection conn = new URL(address).openConnection();
            conn.getContentType();
            // Get host file path.
            
            URL requestUrl = conn.getURL();

            String filename = new File(URLDecoder.decode(requestUrl.getFile(), "utf-8")).getName();

            File storeFile = new File(downloads, filename);
            
            try {
                // Download & Save file.
                System.out.println("DOWNLOAD: " + requestUrl);
                Files.copy(conn.getInputStream(), storeFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
                System.out.println("Download complete: " + storeFile);
            } catch (IOException ex) {
                Logger.getLogger(NewThreads.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (MalformedURLException ex) {
            Logger.getLogger(NewThreads.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(NewThreads.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}