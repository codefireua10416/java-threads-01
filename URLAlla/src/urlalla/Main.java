
package urlalla;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author All
 */
public class Main {

 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        File downloads = new File("Downloads");

        if (!downloads.exists()) {
            downloads.mkdir();
        }

        List<String> playlist = new ArrayList<>();

        try {
            URL url = new URL("http://www.ex.ua/playlist/57718283.m3u");
            //   URL url = new URL("http://www.ex.ua/playlist/101336703.m3u");
            Scanner scanner = new Scanner(url.openStream());

            while (scanner.hasNextLine()) {
                playlist.add(scanner.nextLine());
            }
        } catch (MalformedURLException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }

        ExecutorService threadPool = Executors.newFixedThreadPool(3);

        for (String address : playlist) {
            NewThreads newThread = new NewThreads(address, downloads);
//            Thread thread = new Thread(newThread);
//            thread.start();
            threadPool.execute(newThread);
        }
        
        threadPool.shutdown();
    }
}