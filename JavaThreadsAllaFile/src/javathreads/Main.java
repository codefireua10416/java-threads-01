/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javathreads;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Scanner;

/**
 *
 * @author human
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int countTreads;
        MyRun myRun = new MyRun();
        System.out.println("Enter number of threads");
        Scanner scanner = new Scanner(System.in);
        countTreads = scanner.nextInt();
        
               for (int i = 0; i < countTreads; i++) {
            Thread thread = new Thread(myRun);
            
            thread.start();
        }

       

        
            try {
                Thread.sleep(1);
            } catch (InterruptedException ex) {
                Logger.getLogger(MyRun.class.getName()).log(Level.SEVERE, null, ex);
                return;
            }
        
    }

}
