/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaurl;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author human
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        File downloads = new File("Downloads");

        if (!downloads.exists()) {
            downloads.mkdir();
        }

        List<String> playlist = new ArrayList<>();

        try {
            URL url = new URL("http://www.ex.ua/playlist/98326351.m3u");

            Scanner scanner = new Scanner(url.openStream());

            while (scanner.hasNextLine()) {
                playlist.add(scanner.nextLine());
            }
        } catch (MalformedURLException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }

        // Executors.newFixedThreadPool(3)
        
        for (String address : playlist) {
            try {
                URL url = new URL(address);

                // Get host file path.
                String hostFilename = url.getFile();
                String filename = new File(hostFilename).getName();
                Path storePath = new File(downloads, filename).toPath();

                // Download & Save file.
                Files.copy(url.openStream(), storePath, StandardCopyOption.REPLACE_EXISTING);
            } catch (MalformedURLException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

}
